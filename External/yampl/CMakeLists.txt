# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# Package building YAMPL for ATLAS.
#

# The package's name:
atlas_subdir( yampl )

# External(s) needed by yampl.
find_package( UUID )

# Directory for the temporary build results:
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/yamplBuild" )
# Directory holding the "stamp" files.
set( _stampDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/yamplStamp" )

# Set up the build of YAMPL.
ExternalProject_Add( yampl
   PREFIX "${CMAKE_BINARY_DIR}"
   STAMP_DIR "${_stampDir}"
   URL "http://cern.ch/atlas-software-dist-eos/externals/yampl/yampl-46f7a48.tar.bz2"
   URL_MD5 "85e2c2429e034b61581ef1c45927f0d7"
   BUILD_IN_SOURCE 1
   PATCH_COMMAND patch -p1 <
   "${CMAKE_CURRENT_SOURCE_DIR}/patches/yampl-46f7a48-uuid.patch"
   COMMAND patch -p1 <
   "${CMAKE_CURRENT_SOURCE_DIR}/patches/yampl-46f7a48-targets.patch"
   CONFIGURE_COMMAND "<SOURCE_DIR>/configure" --prefix=${_buildDir}
   CXXFLAGS=-I${UUID_INCLUDE_DIR} LDFLAGS=${UUID_LIBRARIES}
   INSTALL_COMMAND make install
   COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/"
   "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}" )
ExternalProject_Add_Step( yampl forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo
   "Forcing the re-download of yampl (2021.01.04.)"
   DEPENDERS download )
add_dependencies( Package_yampl yampl )

# Install YAMPL:
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Install its find-module:
install( FILES "cmake/Findyampl.cmake"
   DESTINATION "${CMAKE_INSTALL_CMAKEDIR}/modules" )

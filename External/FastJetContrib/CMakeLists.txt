# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# Package building the FastJet contrib libraries as part of the offline
# software build.
#

# Set the package name:
atlas_subdir( FastJetContrib )

# In release recompilation mode stop here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# The source code of FastJetContrib:
set( _source
   "https://cern.ch/lcgpackages/tarFiles/sources/fjcontrib-1.044.tar.gz" )
set( _md5 "19438558d7b727335fab90438620c9ae" )

# Temporary directory for the build results:
set( _buildDir
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/FastJetContribBuild )

# Decide where to pick up FastJet from.
if( ATLAS_BUILD_FASTJET )
   set( FASTJET_CONFIG_SCRIPT ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/fastjet-config )
else()
   find_package( FastJet )
endif()

# Set up the script used for configuring the build of FastJetConntrib.
configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/configure.sh.in
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure.sh @ONLY )

# Set up the build of FastJetContrib for the build area:
ExternalProject_Add( FastJetContrib
   PREFIX ${CMAKE_BINARY_DIR}
   URL "${_source}"
   URL_MD5 "${_md5}"
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   CONFIGURE_COMMAND
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure.sh
   BUILD_IN_SOURCE 1
   BUILD_COMMAND
   ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/atlas_build_run.sh make
   COMMAND
   ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/atlas_build_run.sh make install
   INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory
   ${_buildDir}/ <INSTALL_DIR> )
ExternalProject_Add_Step( FastJetContrib forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo
   "Forcing the re-download of FastJetContrib..."
   DEPENDERS download )
if( ATLAS_BUILD_FASTJET )
   add_dependencies( FastJetContrib FastJet )
endif()
add_dependencies( Package_FastJetContrib FastJetContrib )

# Set up its installation:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

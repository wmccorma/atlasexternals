// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

namespace std
{

template<typename T>
struct deque_iterator
{
  friend deque_iterator
  operator+(const deque_iterator&, unsigned int) 
  {
    deque_iterator __tmp;
    return __tmp;
  }
};

template<typename T>
class deque
{
public:
  typedef deque_iterator<char>	  iterator;
  iterator begin();
  
  iterator insert()
  {
    return begin() + 2;
  }

};


} // namespace std


LibXml2
=======

This package builds the [libxml2](http://xmlsoft.org)
libraries/executables, to be used in standalone/analysis releases.

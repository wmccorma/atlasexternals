# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# CMake configuration building APE as part of the release build.
# To be kept in sync with the requirements file of the package.
#

# Set the name of the package:
atlas_subdir( APE )

# External dependencies:
find_package( TBB )

# In release recompilation mode finish here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Directory to put temporary build results into:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/APEBuild )

# Build the package for the build area:
ExternalProject_Add( APE
   PREFIX ${CMAKE_BINARY_DIR}
   GIT_REPOSITORY https://github.com/samikama/APE.git
   GIT_TAG b47d155
   CMAKE_CACHE_ARGS
   -DTBB_INSTALL_DIR:PATH=${TBB_INSTALL_DIR}
   -DYAMPL_DIR:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   -DBOOST_ROOT:PATH=${BOOST_LCGROOT}
   -DBOOST_INCLUDEDIR:PATH=${BOOST_INCLUDEDIR}
   -DCMAKE_PREFIX_PATH:PATH=${YAMLCPP_LCGROOT}
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DCMAKE_SYSTEM_IGNORE_PATH:STRING=/usr/include;/usr/lib;/usr/lib32;/usr/lib64
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( APE forceconfigure
   COMMAND ${CMAKE_COMMAND} -E echo "Forcing the (re-)configuration of APE"
   COMMAND ${CMAKE_COMMAND} -E remove -f <BINARY_DIR>/CMakeCache.txt
   DEPENDEES update
   DEPENDERS configure
   ALWAYS 1 )
ExternalProject_Add_Step( APE buildinstall
   COMMAND ${CMAKE_COMMAND} -E remove_directory ${_buildDir}/lib/cmake
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR>
   COMMENT "Installing APE into the build area"
   DEPENDEES install )
add_dependencies( APE yampl )
add_dependencies( Package_APE APE )

# Install APE:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Install its find-module:
install( FILES cmake/FindAPE.cmake
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR}/modules )

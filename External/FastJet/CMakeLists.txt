# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# Package building FastJet as part of the offline software build.
#

# Set the package name:
atlas_subdir( FastJet )

# In release recompilation mode stop here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# The source code of FastJet:
set( _source
   "http://cern.ch/lcgpackages/tarFiles/sources/fastjet-3.3.4.tar.gz" )
set( _md5 "19de6f4be3e7cf7dad29e6174d2739cf" )

# Decide whether to request debug symbols from the build:
if( "${CMAKE_BUILD_TYPE}" STREQUAL "Release" OR
      "${CMAKE_BUILD_TYPE}" STREQUAL "MinSizeRel" OR
      "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   set( _fastJetExtraConfig "--disable-debug" )
else()
   set( _fastJetExtraConfig "--enable-debug" )
endif()

# Temporary directory for the build results:
set( _buildDir
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/FastJetBuild )

# Extra environment options for the configuration.
set( _cflags )
set( _ldflags )
if( CMAKE_OSX_SYSROOT )
   list( APPEND _cflags -isysroot ${CMAKE_OSX_SYSROOT}
                        -I${CMAKE_OSX_SYSROOT}/usr/include )
   list( APPEND _ldflags -isysroot ${CMAKE_OSX_SYSROOT} )
endif()

# Massage the options to make them usable in the configuration script.
string( REPLACE ";" " " _cflags "${_cflags}" )
string( REPLACE ";" " " _ldflags "${_ldflags}" )

# Create the helper scripts.
configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/sanitizeFastJet.sh.in
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/sanitizeFastJet.sh
   @ONLY )
configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/configure.sh.in
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure.sh
   @ONLY )

# Set up the build of FastJet for the build area:
ExternalProject_Add( FastJet
   PREFIX ${CMAKE_BINARY_DIR}
   URL "${_source}"
   URL_MD5 "${_md5}"
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   PATCH_COMMAND patch -p1 <
   ${CMAKE_CURRENT_SOURCE_DIR}/patches/thread-local-rng.patch
   CONFIGURE_COMMAND
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure.sh
   INSTALL_COMMAND make install
   COMMAND ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/sanitizeFastJet.sh
   COMMAND ${CMAKE_COMMAND} -E copy_directory
   ${_buildDir}/ <INSTALL_DIR> )
ExternalProject_Add_Step( FastJet forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "Forcing the re-download of FastJet."
   DEPENDERS download )
ExternalProject_Add_Step( FastJet purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for FastJet"
   DEPENDEES download
   DEPENDERS patch )
add_dependencies( Package_FastJet FastJet )

# Set up its installation:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Clean up.
unset( _fastJetExtraConfig )
unset( _buildDir )
unset( _os )
unset( _isValid )
unset( _cflags )
unset( _ldflags )
unset( _source )
unset( _md5 )

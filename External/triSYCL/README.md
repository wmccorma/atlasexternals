triSYCL
=======

This package downloads ("builds") the triSYCL headers, to be used for SYCL
compilation when the compiler itself doesn't provide support for this
language.

https://github.com/triSYCL/triSYCL

# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# Package building CLHEP as part of the offline build procedure.
#

# The name of the package:
atlas_subdir( CLHEP )
cmake_minimum_required( VERSION 3.7 )

# Make sure that all _ROOT variables *are* used when they are set.
if( POLICY CMP0074 )
   cmake_policy( SET CMP0074 NEW )
endif()

# Stop here if CLHEP is not to be built.
if( NOT ATLAS_BUILD_CLHEP )
   return()
endif()

# Let the user know what's happening.
message( STATUS "Building CLHEP as part of this project" )

# Git repository for CLHEP:
set( ATLAS_CLHEP_REPOSITORY "https://gitlab.cern.ch/atlas-sw-git/CLHEP.git"
   CACHE STRING "Repository to fetch CLHEP from" )
# Git tag to build:
set( ATLAS_CLHEP_TAG "CLHEP_2_4_1_3_atl02"
   CACHE STRING "Git tag to use for the CLHEP build" )

# Extra options for the configuration:
set( _extraOptions )
if( "${CMAKE_CXX_STANDARD}" EQUAL 11 )
   list( APPEND _extraOptions -DCLHEP_BUILD_CXXSTD:STRING=-std=c++11 )
elseif( "${CMAKE_CXX_STANDARD}" GREATER_EQUAL 14 ) # CLHEP is not compatible with C++17
   list( APPEND _extraOptions -DCLHEP_BUILD_CXXSTD:STRING=-std=c++14 )
endif()

if( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   list( APPEND _extraOptions -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()

# Directory for the temporary build results:
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/CLHEPBuild" )
# Directory holding the "stamp" files.
set( _stampDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/CLHEPStamp" )

# Build CLHEP in the build area:
ExternalProject_Add( CLHEP
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   STAMP_DIR "${_stampDir}"
   GIT_REPOSITORY "${ATLAS_CLHEP_REPOSITORY}"
   GIT_TAG "${ATLAS_CLHEP_TAG}"
   CMAKE_CACHE_ARGS
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DCLHEP_BUILD_DOCS:BOOL=OFF
   ${_extraOptions}
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( CLHEP cleansource
   COMMAND ${CMAKE_COMMAND} -E remove -f
   "${_stampDir}/CLHEP-gitclone-lastrun.txt"
   DEPENDERS download )
# NB need to modify the printout here in the case of a CLHEP version change
ExternalProject_Add_Step( CLHEP forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "Forcing the re-download of CLHEP (2021/03/10)."
   DEPENDERS cleansource )
ExternalProject_Add_Step( CLHEP purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for CLHEP"
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( CLHEP buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory
   ${_buildDir}/ <INSTALL_DIR>
   COMMENT "Installing CLHEP into the build area"
   DEPENDEES install )
add_dependencies( Package_CLHEP CLHEP )

# Install CLHEP:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

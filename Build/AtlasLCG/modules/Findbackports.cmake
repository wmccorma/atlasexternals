# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Sets:
#  BACKPORTS_PYTHON_PATH
#
# Can be steered by BACKPORTS_LCGROOT.
#

# The LCG include(s).
include( LCGFunctions )

# Find it.
lcg_python_external_module( NAME backports
   PYTHON_NAMES backports/__init__.py backports.py configparser.py )

# Handle the standard find_package arguments.
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( backports DEFAULT_MSG
   _BACKPORTS_PYTHON_PATH )

# Set up the RPM dependency.
lcg_need_rpm( backports )

# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# This file is here to intercept find_package(PythonInterp) calls, and extend
# the environment setup file of the project with the correct Python paths.
#
# This module is deprecated, use FindPython instead.
#

# This module requires CMake 3.12.
cmake_minimum_required( VERSION 3.12 )

# The LCG include(s):
include( LCGFunctions )

# Temporarily clean out CMAKE_MODULE_PATH, so that we could pick up
# FindPythonInterp.cmake from CMake:
set( _modulePathBackup ${CMAKE_MODULE_PATH} )
set( CMAKE_MODULE_PATH )

# Make the code ignore the system path(s). If we are to pick up Python
# from the release.
if( PYTHON_LCGROOT )
   set( CMAKE_SYSTEM_IGNORE_PATH /usr/include /usr/bin /usr/lib /usr/lib32
      /usr/lib64 )
endif()
lcg_system_ignore_path_setup()

# Option for forcing the usage of Python2 when both Python2 and Python3 are
# available.
option( ATLAS_FORCE_PYTHON2 "Force the usage of Python2" FALSE )

# Call CMake's own FindPython.cmake module. It will prefer finding Python3
# over Python2 if it's available, which is generally the behaviour that
# we want, but ATLAS_FORCE_PYTHON2 can be used to force the usage of Python2
# if needed.
if( NOT PYTHONINTERP_FOUND )
   message( DEPRECATION "FindPythonInterp is deprecated. Use FindPython instead." )

   # Decide about the name of the module to use.
   set( _moduleName Python )
   if( ATLAS_FORCE_PYTHON2 )
      set( _moduleName Python2 )
   endif()

   # Find the module of interest.
   find_package( ${_moduleName} COMPONENTS Interpreter )

   # Persistify the variables set by the find module, in a backwards compatible
   # way.
   set( PYTHONINTERP_FOUND "${${_moduleName}_FOUND}" CACHE INTERNAL
      "Python interpreter found" )
   set( PYTHON_VERSION_STRING "${${_moduleName}_VERSION}" CACHE INTERNAL
      "Python version" )
   set( PYTHON_VERSION_MAJOR "${${_moduleName}_VERSION_MAJOR}" CACHE INTERNAL
      "Python major version" )
   set( PYTHON_VERSION_MINOR "${${_moduleName}_VERSION_MINOR}" CACHE INTERNAL
      "Python minor version" )
   set( PYTHON_VERSION_PATCH "${${_moduleName}_VERSION_PATCH}" CACHE INTERNAL
      "Python patch version" )
   set( PYTHON_EXECUTABLE "${${_moduleName}_EXECUTABLE}" CACHE INTERNAL
      "Python executable" )
endif()

# Restore CMAKE_MODULE_PATH:
set( CMAKE_MODULE_PATH ${_modulePathBackup} )
unset( _modulePathBackup )
set( CMAKE_SYSTEM_IGNORE_PATH )

# Set some extra variable(s), to make the environment configuration easier:
if( PYTHON_EXECUTABLE )
   if( NOT PYTHONHOME )
      get_filename_component( PythonInterp_BINARY_PATH ${PYTHON_EXECUTABLE}
         PATH )
      set( PythonInterp_BINARY_PATH "${PythonInterp_BINARY_PATH}" CACHE INTERNAL
         "Python binary/executable path" )
      execute_process( COMMAND
         "${PYTHON_EXECUTABLE}" -c
         "try: import sys; print( sys.prefix )\nexcept:pass\n"
         OUTPUT_VARIABLE PYTHONHOME
         OUTPUT_STRIP_TRAILING_WHITESPACE )
      # Store PYTHONHOME in cache:
      set( PYTHONHOME "${PYTHONHOME}" CACHE INTERNAL "PYTHONHOME" )
   endif()
   if( NOT "${PYTHONHOME}" STREQUAL "" )
      set ( PythonInterp_ENVIRONMENT
         FORCESET PYTHONHOME ${PYTHONHOME} )
   endif()
endif()

# Set up the RPM dependency.
lcg_need_rpm( Python FOUND_NAME ${_moduleName} VERSION_NAME PYTHON )

# Clean up.
unset( _moduleName )

Contributing To The atlasexternals Code
=======================================

The code is managed very similarly to the
[atlas/athena](https://gitlab.cern.ch/atlas/athena) repository. Users wishing
to contribute need to make a fork of it on GitLab, develop their intended
modification in a feature branch of their own fork, and then submit a merge
request into the appropriate branch of this repository from their fork's
feature branch.

For technical details on using forks, and setting up merge requests on GitLab,
see the documentation written for developing code for
[atlas/athena](https://gitlab.cern.ch/atlas/athena), on the
[software documentation](https://atlassoftwaredocs.web.cern.ch/) webpage.

Common Development Procedures
-----------------------------

In the following we discuss some basics about developing code for this
repository. For more general topics on how to use the code of the repository,
see the [README](README.md).

### Adding a New External Package

External projects are built using the CMake built-in
[ExternalProject_Add](https://cmake.org/cmake/help/v3.2/module/ExternalProject.html)
function. For each external project we set up a separate sub-directory
under [External/](External/).

To write one new such package/subdirectory, you have to:
 * Write a `CMakeLists.txt` file for it that takes care of building and
   installing the external "in the right way". Installing the external's files
   under `${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}` in the build directory, and
   into the main installation directory during the installation step. So that
   the headers and libraries of all externals would end up beside each other.
   (Making the environment that much smaller when setting up the release built
   on top of the externals project.)
 * Write a README.md file for the external giving a short description of its
   purpose, and any other information that may be relevant for the maintenance
   of that subdirectory.
 * Update all the projects that should build this new external to include it
   in their `package_filters.txt` files.

### Adding a New External Project

All the different projects that can be built out of this repository are kept
under [Projects/](Projects/). In case you need to set up a new project, first
of all you should discuss about this in an appropriate meeting. Since setting
up new projects is usually a non-trivial effort.

In case the agreement is to indeed set up a new project, have a look at the
setup of all the existing ones. Like
[Projects/AthenaExternals](Projects/AthenaExternals) or
[Projects/AthSimulationExternals](Projects/AthSimulationExternals). (The
analysis projects are generally a bit more tricky.) And try to implement your
own project based on those.

### Maintaining Code in the Repository

Some actual code is also kept in this repository. Like
[External/CheckerGccPlugins](External/CheckerGccPlugins). Such packages need to
be maintained exactly like packages in the
[atlas/athena](https://gitlab.cern.ch/atlas/athena) repository.

Copyright
---------

Note that all source files in the repository need to carry the following
copyright statement at their top:

```
Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
```
